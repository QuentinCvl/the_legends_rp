<?php

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>The Legends RP</title>
    <link rel="icon" href="favicon.ico">
    <!-- Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Personal CSS -->
    <link type="text/css" rel="stylesheet" href="homepage.css">
</head>
<body>
<div class="full-height">

    <div class="logo">
        <img src="logo-128x128.png">
    </div>

    <div class="menu">
        <div style="text-align: center">
            <h1>Bienvenue sur The Legends RP</h1>
        </div>
        <div class="link">
            <a href="https://www.thelegendsrp.fr/wiki/index.php">
                <button type="button" class="btn btn-secondary btn-lg">Wiki</button>
            </a>

            <a href="https://www.thelegendsrp.fr/panel/index.php">
                <button type="button" class="btn btn-secondary btn-lg">Panel</button>
            </a>
        </div>

    </div>


</div>

<!-- BOOTSTRAP SCRIPT-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
