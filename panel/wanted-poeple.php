<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Avis de recherche : Personnes</h1>
  </div>

  <!-- Content Row -->
  <div class="row" style="justify-content: space-evenly; margin-bottom: 4%;">
    <a href="#showWantedPoeple" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Voir les avis de recherche
    </a>
    <a href="#newWantedPoeple" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Créer un avis de recherche
    </a>
  </div>

  <hr class="sidebar-divider my-3">

  <?php
  if (isset($_POST['create-wanted-poeple'])) {
    $new_wanted_poeple = new WantedPoeple($_POST['wanted_people_firstname'], $_POST['wanted_people_lastname']);
    $new_wanted_poeple->createWantedPoeple($_POST['wanted_people_description'], $_POST['wanted_people_complain'], $_POST['createdby']);
  }
  if (isset($_POST['update-wanted-people'])) {
    $update_cases = new WantedPoeple($_POST['update_wanted_people_firstname'], $_POST['update_wanted_people_lastname']);
    $update_cases->updateWantedPoeple($_POST['wanted_poeple_id'], $_POST['update_wanted_people_description'], $_POST['update_wanted_people_complain']);
  }
  if (isset($_POST['delete-wanted-poeple'])) {
    $delete_cases = new WantedPoeple($_POST['update_wanted_people_firstname'], $_POST['update_wanted_people_firstname']);
    $delete_cases->deleteWantedPoeple($_POST['wanted_poeple_id']);
  }
  ?>
  <!-- Show record -->
  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="showWantedPoeple">
      <h2 class="mb-4">Personnes recherchées</h2>
      <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Liste des personnes recherchées</h6>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="users" style="width='100%'; cellspacing='0'">
                <thead> <!-- Apparaitra en haut -->
                <tr>
                  <th class="red">N°</th>
                  <th class="red">Auteur</th>
                  <th class="red">Identité</th>
                  <th class="red">Créé le</th>
                  <th class="red">Voir</th>
                </tr>
                </thead>
                <?php
                $BDD = new BDD();
                $dbh = $BDD->getConnection();
                $stmt = $dbh->query("SELECT * FROM `wanted-poeple`");

                echo '<tbody>';
                foreach ($stmt as $row) {
                  echo '<tr><form method="post" action="index.php?page=show-wanted-poeple" > <!-- Contenu, géré via bdd -->
                          <td>' . $row['id'] . '</td>
                          <td>' . $row['created_by'] . '</td>
                          <td>' . $row['firstname'].' '.$row['lastname'] . '</td>
                          <td>' . $row['created_at'] . '</td>
                          <input type="hidden" value="' . $row['id'] . '" name="currentId">
                          <td style="text-align: center">
                            <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm modifUser" name="showWantedPoeple"></button>
                          </td>
                        </form></tr>';
                }

                echo '</tbody>';
                ?>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="newWantedPoeple">
      <h2 class="mb-4">Créer un avis de recherche</h2>
      <form method="post" action="index.php?page=wanted-poeple">
        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="wanted_people_firstname">Prénom*</span>
          </div>
          <input type="text" class="form-control" placeholder="Prénom" required
                 aria-describedby="wanted_people_firstname" name="wanted_people_firstname">
          <input type="hidden" name="createdby"
                 value="<?php echo $_SESSION['grade'] . ' ' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname'] ?>">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="wanted_people_lastname">Nom*</span>
          </div>
          <input type="text" class="form-control" placeholder="Nom" required
                 aria-describedby="wanted_people_lastname" name="wanted_people_lastname">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Description<br/>physique*</span>
          </div>
          <textarea maxlength="65000" required class="form-control" name="wanted_people_description"
                    placeholder="Notez ici toute les particularité physique de l'individu recherché"></textarea>
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Faits reprochés*</span>
          </div>
          <textarea maxlength="65000" required class="form-control" name="wanted_people_complain"
                    placeholder="Ecrivez ici les faits reprochés de l'individu qui lui valent sont avis de recherche"></textarea>
        </div>

        <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm"
                  name="create-wanted-poeple">Valider
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
