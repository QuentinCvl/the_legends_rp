<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Rapport en Cours</h1>
  </div>

  <!-- Content Row -->
  <div class="row" style="justify-content: space-evenly; margin-bottom: 4%;">
    <a href="#newCases" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Créer un nouveau rapport
    </a>
    <a href="#showCases" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Voir les rapports en cours
    </a>
  </div>

  <hr class="sidebar-divider my-3">

  <?php
  if (isset($_POST['create-cases-button'])) {
    $new_cases = new Cases($_POST['cases_title'], $_POST['cases_content']);
    $new_cases->createCases($_POST['createdby'], $_POST['cases_charged_personnel'], $_POST['cases_reason']);
  }
  if (isset($_POST['update-cases-button'])) {
    $update_cases = new Cases($_POST['update_cases_title'], $_POST['update_cases_content']);
    $update_cases->updateCases($_POST['update_cases_id'], $_POST['update_charged_personnel'], $_POST['update_cases_reason'],
        $_POST['update_by']);
  }
  if (isset($_POST['resolve-cases-button'])) {
    $update_cases = new Cases($_POST['update_cases_title'], $_POST['update_cases_content']);
    $update_cases->resolveCases($_POST['update_cases_id']);
  }
  if (isset($_POST['close-cases-button'])) {
    $update_cases = new Cases($_POST['update_cases_title'], $_POST['update_cases_content']);
    $update_cases->closeCases($_POST['update_cases_id']);
  }
  if (isset($_POST['delete-cases-button'])) {
    $delete_cases = new Cases($_POST['update_cases_title'], $_POST['update_cases_content']);
    $delete_cases->deleteCases($_POST['update_cases_id']);
  }
  if (isset($_POST['cases-add-img'])) {
    $targetDir = "data/cases-img/";
    $allowTypes = array('jpg', 'png', 'jpeg');
    $fileNames = array_filter($_FILES['cases_img']['name']);
    if (!empty($fileNames)) {
      foreach ($_FILES['cases_img']['name'] as $key => $val) {
        $fileName = basename($_FILES['cases_img']['name'][$key]);
        $targetFilePath = $targetDir . $fileName;
        $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
        if (in_array($fileType, $allowTypes)) {
          if (move_uploaded_file($_FILES["cases_img"]["tmp_name"][$key], $targetFilePath)) { // upload this file to the target
            $cases_img = new CasesImg($_POST['cases-id'], $_POST['update_by']); // add this to bdd
            $cases_img->addNewImg($targetFilePath);
          } else {
            echo '<div class="alert alert-danger float" role="alert">' .
                "Les preuves images n'ont pas pu être envoyées." .
                '</div>';
          }
        } else {
          echo '<div class="alert alert-danger float" role="alert">' .
              "Une des images ne respecte pas le type d'extension autorisé,<br/>
              les types autorisés sont : JPG, JPEG et PNG." .
              '</div>';
        }
      }

    } else {
      echo '<div class="alert alert-danger float" role="alert">' .
          "Il semblerais que vous n'ayez pas selectionné d'image." .
          '</div>';
    }
  }
  if (isset($_POST['delete-img'])) {
    $file = $_POST['delete-src'];
    if (!unlink($file)) {
      echo '<div class="alert alert-success float" role="alert">' .
          "Impossible de supprimé l'image, contacté un administrateur." .
          '</div>';
    } else {
      $id = $_POST['delete-img'];
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $req = $dbh->query("DELETE FROM `cases-img` WHERE id ='$id'");


      echo '<div class="alert alert-success float" role="alert">' .
          "L'image a bien était supprimé." .
          '</div>';
    }
  }
  ?>

  <div class="col-xl-12 col-md-12 mb-12" id="newCases">
    <h2 class="mb-4">Créer un rapport</h2>
    <form method="post" action="index.php?page=ongoing-cases">
      <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
        <div class="input-group-prepend">
          <span class="input-group-text" id="cases_title">Titre*</span>
        </div>
        <input type="text" class="form-control" placeholder="Titre" aria-label="title" required
               aria-describedby="cases_title" name="cases_title">
        <input type="hidden" name="createdby"
               value="<?php echo $_SESSION['grade'] . ' ' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname'] ?>">
      </div>

      <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
        <div class="input-group-prepend">
          <span class="input-group-text">Chargé de l'affaire*</span>
        </div>
        <textarea maxlength="65000" required class="form-control" aria-label="charged_personnel"
                  name="cases_charged_personnel"
                  placeholder="Ecrivez ici le nom des agents en charge de l'affaire"></textarea>
      </div>

      <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
        <div class="input-group-prepend">
          <span class="input-group-text">Objet de l'affaire*</span>
        </div>
        <textarea maxlength="65000" required class="form-control" aria-label="reason" name="cases_reason"
                  placeholder="Ecrivez ici les délis lié a l'affaire"></textarea>
      </div>

      <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
        <div class="input-group-prepend">
          <span class="input-group-text">Contenu*</span>
        </div>
        <textarea maxlength="65000" required class="form-control" aria-label="content" name="cases_content"
                  placeholder="Ecrivez ici"></textarea>
      </div>

  </div>

  <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
    <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm"
            name="create-cases-button">Valider
    </button>
  </div>
  </form>

  <!-- Show record -->
  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="showCases">
      <h2 class="mb-4">Rapport en cours</h2>
      <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Liste des rapports en cours</h6>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="users" style="width='100%'; cellspacing='0'">
                <thead> <!-- Apparaitra en haut -->
                <tr>
                  <th class="red">N°</th>
                  <th class="red">Auteur</th>
                  <th class="red">Titre</th>
                  <th class="red">Créé le</th>
                  <th class="red">Voir</th>
                </tr>
                </thead>
                <?php
                $status = "ongoing";
                $BDD = new BDD();
                $dbh = $BDD->getConnection();
                $stmt = $dbh->query("SELECT * FROM cases WHERE cases_status = '$status'");

                echo '<tbody>';
                foreach ($stmt as $row) {
                  echo '<tr><form method="post" action="index.php?page=show-ongoing-cases" > <!-- Contenu, géré via bdd -->
                          <td>' . $row['id'] . '</td>
                          <td>' . $row['created_by'] . '</td>
                          <td>' . $row['title'] . '</td>
                          <td>' . $row['created_at'] . '</td>
                          <input type="hidden" value="' . $row['id'] . '" name="currentId">
                          <td style="text-align: center">
                            <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm modifUser" name="showCases"></button>
                          </td>
                        </form></tr>';
                }

                echo '</tbody>';
                ?>
              </table>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
</div>
