<?php
if (!isset($_SESSION['grade'])) { // Si la personne n'est plus connecté, elle est redirigé vers la page de connexion
  echo '<script> document.location.replace("index.php");</script>';
}
if (isset($_POST['showWantedPoeple'])) {
  $BDD = new BDD();
  $dbh = $BDD->getConnection();
  $req = $dbh->prepare('SELECT * FROM `wanted-poeple` WHERE id = ?');
  $req->execute(array($_POST['currentId']));
  $req = $req->fetch(PDO::FETCH_ASSOC);
}
?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Avis de recherche</h1>
  </div>
  <p>Avis N° <?php echo $req['id'] ?></p>
  <p>Créé par <?php echo $req['created_by'] ?></p>
  <p>Le <?php echo $req['created_at'] ?></p>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h2 class="h2 mb-0">Information sur l'individu</h2>
    </div>

    <div class="col-xl-12 col-md-12 mb-12">
      <h4 class="mb-4">Indentité de la personne recherché :
        <span style="color: black">
          <?php echo $req['firstname'] . ' ' . $req['lastname'] ?>
        </span>
      </h4>

    </div>

    <div class="col-xl-12 col-md-12 mb-12">
      <h4 class="mb-4" style="color: black">Description physique</h4>
      <div class="container-fluid">
        <?php echo nl2br($req['physical_description']) ?>
      </div>
    </div>


    <hr class="sidebar-divider my-3">


    <div class="col-xl-12 col-md-12 mb-12">
      <h4 class="mb-4" style="color: black">Fait reproché</h4>
      <div class="container-fluid">
        <?php echo nl2br($req['complained']) ?>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">


  <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
    <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm"
            id="showUpdateCriminalRecord">
      Modifier
    </button>
  </div>

  <div class="row" style="display: none" id="updateCriminalRecord">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Modifier l'avis de recherche</h2>
      <form method="post" action="index.php?page=wanted-poeple">
        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="update_wanted_people_firstname">Prénom</span>
          </div>
          <input type="text" class="form-control" placeholder="Prénom" required
                 aria-describedby="update_wanted_people_firstname" name="update_wanted_people_firstname"
                value="<?php echo $req['firstname'] ?>">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="update_wanted_people_lastname">Nom</span>
          </div>
          <input type="text" class="form-control" placeholder="Nom" required
                 aria-describedby="update_wanted_people_lastname" name="update_wanted_people_lastname"
                 value="<?php echo $req['lastname'] ?>">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Description<br/>physique</span>
          </div>
          <textarea maxlength="65000" required class="form-control" name="update_wanted_people_description"
                    style="min-height: 150px;"><?php
            echo $req['physical_description'];
            ?></textarea>
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Faits reprochés</span>
          </div>
          <textarea maxlength="65000" required class="form-control" name="update_wanted_people_complain"
                    style="min-height: 150px;"><?php
            echo $req['complained'];
            ?></textarea>
          <input type="hidden" value="<?php echo $req['id'] ?>" name="wanted_poeple_id">
        </div>
        <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm"
                  name="update-wanted-people">
            Valider
          </button>
        </div>
        <?php
        if ($_SESSION['grade'] == "Administrateur" || $_SESSION['grade'] == 'Commandant' || $_SESSION['grade'] == 'Capitaine' || $_SESSION['grade'] == 'Shériff' || $_SESSION['grade'] == 'Shériff-Adjoint' || $_SESSION['grade'] == 'Gouverneur') {
          echo '<div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" name="delete-wanted-poeple">
            Supprimer
          </button>
        </div>';
        }
        ?>
    </div>

    </form>
  </div>
</div>

</div>

<script>

    $('#showUpdateCriminalRecord').on('click', function () {
        var show = document.getElementById('updateCriminalRecord');
        show.style.display = "block";
        var hide = document.getElementById('showUpdateCriminalRecord');
        hide.className = "d-none-btn";
    })
</script>
