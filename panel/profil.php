<?php

?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Profil</h1>
  </div>

  <!-- Content Row -->
  <div class="row" style="justify-content: space-evenly; margin-bottom: 4%;">
    <a href="#general_information" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Modifier les informations générales
    </a>
    <a href="#modif" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Modifier le mot de passe
    </a>
  </div>
  <?php
  if (isset($_POST['profil_update_username'])) {
    $new_user = new Users($_POST['profil_update_username'], $_SESSION['pswd']);
    $new_user->profilUpdate($_POST['profil_update_firstname'], $_POST['profil_update_lastname']);
  }
  if (isset($_POST['profil_current_pswd'])) {
    $new_user = new Users($_SESSION['username'], ($_POST['profil_current_pswd']));
    if ($_POST['profil_new_pswd'] == $_POST['profil_new_pswd2'])
      $new_user->profilUpdatePswd($_POST['profil_new_pswd']);
  }
  ?>
  <hr class="sidebar-divider my-3">
  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="general_information">
      <h2 class="mb-4">Informations Générales</h2>
      <form method="post" action="index.php?page=profil">
        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="profil_update_username">Username</span>
          </div>
          <input type="text" class="form-control" aria-label="Username"
                 aria-describedby="profil_update_username" name="profil_Affiche_username" disabled
                 value="<?php echo $_SESSION['username'] ?>">
          <input type="hidden" name="profil_update_username" value="<?php echo $_SESSION['username'] ?>">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="profil_update_firstname">Prénom</span>
          </div>
          <input type="text" class="form-control" aria-label="Firstname" required
                 aria-describedby="profil_update_firstname" name="profil_update_firstname"
                 value="<?php echo $_SESSION['firstname'] ?>">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="profil_update_lastname">Nom</span>
          </div>
          <input type="text" class="form-control" aria-label="Lastname" required
                 aria-describedby="profil_update_lastname" name="profil_update_lastname"
                 value="<?php echo $_SESSION['lastname'] ?>">
        </div>

        <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm">
            Valider
          </button>
        </div>
      </form>


      <hr class="sidebar-divider my-3">
      <div class="row">
        <div class="col-xl-12 col-md-12 mb-12" id="general_information">
          <h2 class="mb-4">Mot de Passe</h2>
          <form method="post" action="index.php?page=profil">
            <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
              <div class="input-group-prepend">
                <span class="input-group-text" id="profil_current_pswd">Ancien Mdp</span>
              </div>
              <input type="password" class="form-control" placeholder="Mot de Passe" aria-label="Mot de Passe" required
                     autocomplete aria-describedby="profil_current_pswd" id="input_profil_current_pswd"
                     name="profil_current_pswd">
              <div class="input-group-append">
                <div class="input-group-text">
                  <input type="checkbox" aria-label="Show Password" id="checkbox_profil_current_pswd">
                </div>
              </div>
            </div>

            <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
              <div class="input-group-prepend">
                <span class="input-group-text" id="profil_new_pswd">Nouveau Mdp</span>
              </div>
              <input type="password" class="form-control" placeholder="Mot de Passe" aria-label="Mot de Passe" required
                     autocomplete aria-describedby="profil_new_pswd" id="input_profil_new_pswd" name="profil_new_pswd">
              <div class="input-group-append">
                <div class="input-group-text">
                  <input type="checkbox" aria-label="Show Password" id="checkbox_profil_new_pswd">
                </div>
              </div>
            </div>

            <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
              <div class="input-group-prepend">
                <span class="input-group-text" id="profil_new_pswd2">Nouveau Mdp</span>
              </div>
              <input type="password" class="form-control" placeholder="Mot de Passe" aria-label="Mot de Passe" required
                     autocomplete aria-describedby="profil_new_pswd2" id="input_profil_new_pswd2"
                     name="profil_new_pswd2">
              <div class="input-group-append">
                <div class="input-group-text">
                  <input type="checkbox" aria-label="Show Password" id="checkbox_profil_new_pswd2">
                </div>
              </div>
            </div>

            <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
              <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm">
                Valider
              </button>
            </div>
          </form>
        </div>
      </div>

    </div>
    <!-- End of Main Content -->
    <script>
        $(document).ready(function () {
            $('#checkbox_profil_current_pswd').on('change', function () {
                var input = document.getElementById('input_profil_current_pswd');

                if (this.checked) {
                    input.setAttribute("type", "text");
                } else {
                    input.setAttribute("type", "password");
                }

            })
            $('#checkbox_profil_new_pswd').on('change', function () {
                var input = document.getElementById('input_profil_new_pswd');

                if (this.checked) {
                    input.setAttribute("type", "text");
                } else {
                    input.setAttribute("type", "password");
                }

            })
            $('#checkbox_profil_new_pswd2').on('change', function () {
                var input = document.getElementById('input_profil_new_pswd2');

                if (this.checked) {
                    input.setAttribute("type", "text");
                } else {
                    input.setAttribute("type", "password");
                }

            })
        })
    </script>
