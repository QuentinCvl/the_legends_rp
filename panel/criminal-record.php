<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Casiers Judidicaire</h1>
  </div>

  <!-- Content Row -->
  <div class="row" style="justify-content: space-evenly; margin-bottom: 4%;">
    <a href="#modifCasier" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Voir les casiers
    </a>
    <a href="#createCasier" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Créer un casier
    </a>
  </div>

  <hr class="sidebar-divider my-3">

  <?php
  if (isset($_POST['create-criminal-record'])) {
    $target_dir = "data/criminal-record-img/";
    $imageFileType = strtolower(pathinfo($_FILES["criminal_img"]['name'],PATHINFO_EXTENSION));
    $target_file = $target_dir . $_POST['criminal_firstname'] . $_POST['criminal_lastname'] .'.'. $imageFileType;
    move_uploaded_file($_FILES["criminal_img"]["tmp_name"], $target_file);
    $new_criminal_record = new Criminal_Record($_POST['criminal_firstname'], $_POST['criminal_lastname']);
    $new_criminal_record->criminalRegister($_POST['createdby'], $_POST['criminal_birthday'], $_POST['criminal_adress']
        , $_POST['criminal_organisation'], $_POST['criminal_condamnation'], $_POST['criminal_sentence'], $target_file);
  }
  if (isset($_POST['update-criminal-record-button'])) {
    $update_criminal_record = new Criminal_Record($_POST['update-criminal-firstname'], $_POST['update-criminal-lastname']);
    $update_criminal_record->criminalRecordUpdate($_POST['update_criminal_adress'], $_POST['update_criminal_organisation'],
        $_POST['update_criminal_condamnation'], $_POST['update_criminal_sentence'], $_POST['update_criminal_updateby']);
  }
  if (isset($_POST['delete-criminal-record-button'])) {
    $delete_criminal_record = new Criminal_Record($_POST['update-criminal-firstname'], $_POST['update-criminal-lastname']);
    $delete_criminal_record->criminalRecordDelete();
  }
  ?>
  <!-- Show record -->
  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="modifCasier">
      <h2 class="mb-4">Voir les Casiers Judiciaire</h2>
      <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Liste des Casiers</h6>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="users" style="width='100%'; cellspacing='0'">
                <thead> <!-- Apparaitra en haut -->
                <tr>
                  <th class="red">N°</th>
                  <th class="red">Auteur</th>
                  <th class="red">Identité</th>
                  <th class="red">Date de<br>naissance</th>
                  <th class="red">Créé le</th>
                  <th class="red">Modifier le</th>
                  <th class="red">Voir</th>
                </tr>
                </thead>
                <?php
                $BDD = new BDD();
                $dbh = $BDD->getConnection();
                $stmt = $dbh->query('SELECT * FROM criminal_record ');

                echo '<tbody>';
                foreach ($stmt as $row) {
                  echo '<tr><form method="post" action="index.php?page=individual-criminal-record" > <!-- Contenu, géré via bdd -->
                          <td>' . $row['id'] . '</td>
                          <td>' . $row['created_by'] . '</td>
                          <td>' . $row['firstname'].' '. $row['lastname'] . '</td>
                          <td>' . $row['birthday'] . '</td>
                          <td>' . $row['created_at'] . '</td>
                          <td>';
                  if (empty($row['update_at'])) echo 'Jamais';
                  else {
                    echo $row['update_at'];
                  }
                  echo '</td>
                          <input type="hidden" value="' . $row['id'] . '" name="currentId">
                          <td style="text-align: center">
                            <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm modifUser" name="showPersonalRecord"></button>
                           </td>
                        </form></tr>';
                }

                echo '</tbody>';
                ?>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="createCasier">
      <h2 class="mb-4">Créer un casier</h2>
      <form method="post" action="index.php?page=criminal-record" enctype="multipart/form-data">
        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="criminal_firstname">Prénom</span>
          </div>
          <input type="text" class="form-control" placeholder="Prénom" aria-label="firstname" required
                 aria-describedby="criminal_firstname" name="criminal_firstname">
          <input type="hidden" name="createdby"
                 value="<?php echo $_SESSION['grade'] . ' ' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname'] ?>">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="criminal_lastname">Nom</span>
          </div>
          <input type="text" class="form-control" placeholder="Nom" aria-label="Lastname" required
                 aria-describedby="criminal_lastname" name="criminal_lastname">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="criminal_img">Photo</span>
          </div>
          <input type="file" class="form-control" aria-label="Image" accept=".png, .jpeg, .jpg"
                 aria-describedby="criminal_img" name="criminal_img" required>
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="criminal_birthday">Date de naissance</span>
          </div>
          <input type="date" class="form-control" placeholder="Nom" aria-label="Birthday" required
                 aria-describedby="criminal_birthday" name="criminal_birthday"  id="criminal_birthday" min="1920-01-01"
                max="<?php echo date('Y-m-d'); ?>">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="criminal_adress">Adresse</span>
          </div>
          <input type="text" class="form-control" placeholder="Adresse" aria-label="Adress" required
                 aria-describedby="criminal_adress" name="criminal_adress">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="criminal_organisation">Membre du Organisation<br>Si non, mettez "inconnue"</span>
          </div>
          <input type="text" class="form-control" placeholder="Nom de l'organisation" aria-label="Organisation" required
                 aria-describedby="criminal_organisation" name="criminal_organisation">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Condamnation</span>
          </div>
          <textarea maxlength="65000" required class="form-control" aria-label="content" name="criminal_condamnation"
                    placeholder="Fait reproché"></textarea>
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Peine encourue(s)</span>
          </div>
          <textarea maxlength="65000" required class="form-control" aria-label="content" name="criminal_sentence"
                    placeholder="X$ d'amande, X années de prisons etc ..."></textarea>
        </div>

        <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm" name="create-criminal-record">
            Valider
          </button>
        </div>
      </form>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script>
    $(document).ready(function () {
        var date = document.getElementById('criminal_birthday')
        var today = new Date().toString();
        date.setAttribute('max', today);
    }

</script>
