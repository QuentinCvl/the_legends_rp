<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Avis de recherche : Véhicules</h1>
  </div>

  <!-- Content Row -->
  <div class="row" style="justify-content: space-evenly; margin-bottom: 4%;">
    <a href="#showWantedVehicles" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Voir les avis de recherche
    </a>
    <a href="#newWantedVehicules" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Créer un avis de recherche
    </a>
  </div>

  <hr class="sidebar-divider my-3">

  <?php
  if (isset($_POST['create-wanted-vehicules'])) {
    $new_wanted_poeple = new WantedVehicles($_POST['wanted_vehicules_numberplate'], $_POST['wanted_vehicules_mark'],
        $_POST['wanted_vehicules_model'], $_POST['wanted_vehicules_color']);
    $new_wanted_poeple->createWantedVehicle($_POST['createdby'], $_POST['wanted_vehicules_reason']);
  }
  if (isset($_POST['update-wanted-vehicles'])) {
    $update_vehicule = new WantedVehicles($_POST['update_wanted_vehicules_numberplate'], $_POST['update_wanted_vehicules_mark'],
        $_POST['update_wanted_vehicules_model'], $_POST['update_wanted_vehicules_color']);
    $update_vehicule->updateWantedVehicles($_POST['wanted_vehicles_id'], $_POST['update_wanted_vehicules_reason']);
  }
  if (isset($_POST['delete-wanted-vehicles'])) {
    $delete_vehicles = new WantedVehicles($_POST['update_wanted_vehicules_numberplate'], $_POST['update_wanted_vehicules_mark'],
        $_POST['update_wanted_vehicules_model'], $_POST['update_wanted_vehicules_color']);
    $delete_vehicles->deleteWantedVehicles($_POST['wanted_vehicles_id']);
  }
  ?>
  <!-- Show record -->
  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="showWantedVehicles">
      <h2 class="mb-4">Véhicles recherchées</h2>
      <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Liste des véhicules recherchées</h6>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="users" style="width='100%'; cellspacing='0'">
                <thead> <!-- Apparaitra en haut -->
                <tr>
                  <th class="red">N°</th>
                  <th class="red">Auteur</th>
                  <th class="red">Plaque</th>
                  <th class="red">Modèle</th>
                  <th class="red">Couleur</th>
                  <th class="red">Créé le</th>
                  <th class="red">Voir</th>
                </tr>
                </thead>
                <?php
                $BDD = new BDD();
                $dbh = $BDD->getConnection();
                $stmt = $dbh->query("SELECT * FROM `wanted-vehicles`");

                echo '<tbody>';
                foreach ($stmt as $row) {
                  echo '<tr><form method="post" action="index.php?page=show-wanted-vehicles" > <!-- Contenu, géré via bdd -->
                          <td>' . $row['id'] . '</td>
                          <td>' . $row['created_by'] . '</td>
                          <td>' . $row['numberplate'] . '</td>
                          <td>' . $row['model'] . '</td>
                          <td>' . $row['color'] . '</td>
                          <td>' . $row['created_at'] . '</td>
                          <input type="hidden" value="' . $row['id'] . '" name="currentId">
                          <td style="text-align: center">
                            <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm modifUser" name="showWantedVehicles"></button>
                          </td>
                        </form></tr>';
                }

                echo '</tbody>';
                ?>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="newWantedVehicules">
      <h2 class="mb-4">Créer un avis de recherche</h2>
      <form method="post" action="index.php?page=wanted-vehicles">
        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="wanted_vehicules_numberplate">Plaque<br/>d'immatriculation</span>
          </div>
          <input type="text" class="form-control" placeholder="Plaque" required
                 name="wanted_vehicules_numberplate">
          <input type="hidden" name="createdby"
                 value="<?php echo $_SESSION['grade'] . ' ' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname'] ?>">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="wanted_vehicules_mark">Marque</span>
          </div>
          <input type="text" class="form-control" placeholder="Marque du véhicule" required
                 name="wanted_vehicules_mark">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="wanted_vehicules_model">Modèle</span>
          </div>
          <input type="text" class="form-control" placeholder="Modèle du véhicule" required
                 name="wanted_vehicules_model">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="wanted_vehicules_color">Couleur</span>
          </div>
          <input type="text" class="form-control" placeholder="Couleur du véhicule" required
                 name="wanted_vehicules_color">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Raison</span>
          </div>
          <textarea maxlength="65000" required class="form-control" name="wanted_vehicules_reason"
                    placeholder="Ecrivez ici les raison de cette avis"></textarea>
        </div>

        <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm"
                  name="create-wanted-vehicules">Valider
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
