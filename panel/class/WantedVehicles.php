<?php


class WantedVehicles {
  public $numberplate;
  public $mark;
  public $model;
  public $color;

  public function __construct($numberplate, $mark, $model, $color) {
    $this->numberplate = $numberplate;
    $this->mark = $mark;
    $this->model = $model;
    $this->color = $color;
  }

  public function createWantedVehicle($createdby, $reason) {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $create = $dbh->prepare('SELECT numberplate FROM `wanted-vehicles` WHERE numberplate = :plate ');
    $create->execute(array(':plate' => $this->numberplate));
    $count = $create->rowCount();

    if ($count != 0) {
      echo '<div class="alert alert-danger" role="alert">' .
          "Un avis de recherche pour ce véhicule existe déja." .
          '</div>';
    } else {
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $sth = $dbh->prepare('INSERT INTO `wanted-vehicles`(numberplate, mark, model, color, reason, created_by) 
            VALUES (:plate, :mark, :model, :color, :reason, :createdby)');
      $sth->bindParam(':plate', $this->numberplate);
      $sth->bindParam(':mark', $this->mark);
      $sth->bindParam(':model', $this->model);
      $sth->bindParam(':color', $this->color);
      $sth->bindParam(':reason', $reason);
      $sth->bindParam(':createdby', $createdby);

      $sth->execute();

      echo '<div class="alert alert-success float" role="alert">' .
          "L'avis de recherche a bien était créé." .
          '</div>';
    }
  }

  public function updateWantedVehicles($id, $reason) {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $test = $dbh->prepare('UPDATE `wanted-vehicles` SET numberplate = :plate, mark = :mark, 
                           model = :model, color = :color, reason = :reason WHERE id = :id');
    $test->execute(array(':plate' => $this->numberplate, ':mark' => $this->mark, ':model' => $this->model,
        ':color' => $this->color, ':reason' => $reason, ':id' => $id));

    echo '<div class="alert alert-success float" role="alert">' .
        "L'avis de recherche a bien était mise a jour." .
        '</div>';
  }

  public function deleteWantedVehicles($id){
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $req = $dbh->query("DELETE FROM `wanted-vehicles` WHERE id ='$id'");
    echo '<div class="alert alert-success float" role="alert">' .
        "L'avis de recherche a bien était supprimé." .
        '</div>';
    return $req;
  }

}
