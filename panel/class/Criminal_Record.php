<?php


class Criminal_Record {
 public $firstname;
 public $lastname;

  public function __construct($firstname, $lastname) {
    $this->firstname = $firstname;
    $this->lastname = $lastname;
  }

  public function criminalRegister($createdby, $birthday, $adress, $organisation, $condamnation, $sentence, $img){
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $test = $dbh->prepare('SELECT firstname AND lastname FROM criminal_record WHERE firstname = :firstname AND lastname = :lastname');
    $test->execute(array(':firstname' => $this->firstname, ':lastname' => $this->lastname));
    $nbr = $test->rowCount();

    if ($nbr != 0) { // Si l'username est deja utilisé, le compteur va passé a 1
      echo '<div class="alert alert-danger" role="alert">' .
          "Un Casier existe dèja à ce nom, merci de le modifier plutôt que d'en créé un nouveau" .
          '</div>';
    } else {
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $sth = $dbh->prepare('INSERT INTO criminal_record(created_by, firstname, lastname, birthday, adress, organisation,
                            condamnation, sentence, img) 
            VALUES (:createdby , :firstname, :lastname, :birthday, :adress, :organisation, :condamnation, :sentence, :img)');
      $sth->bindParam(':createdby', $createdby);
      $sth->bindParam(':firstname', $this->firstname);
      $sth->bindParam(':lastname', $this->lastname);
      $sth->bindParam(':birthday', $birthday);
      $sth->bindParam(':adress', $adress);
      $sth->bindParam(':organisation', $organisation);
      $sth->bindParam(':condamnation', $condamnation);
      $sth->bindParam(':sentence', $sentence);
      $sth->bindParam(':img', $img);
      $sth->execute();

      echo '<div class="alert alert-success float" role="alert">' .
          "Le Casier a bien était créé." .
          '</div>';
    }

  }

  public function criminalRecordUpdate($adress, $organisation, $condamnation, $sentence, $updateby) {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $test = $dbh->prepare('UPDATE criminal_record SET adress = :adress, organisation = :organisation, condamnation = :condamnation,
        sentence = :sentence, update_by = :updateby, update_at = CURRENT_TIMESTAMP() WHERE firstname = :firstname AND lastname = :lastname');
    $test->execute(array(':adress' => $adress, ':organisation' => $organisation, ':condamnation' => $condamnation,
        ':sentence' => $sentence, ':updateby' => $updateby, ':firstname' => $this->firstname, ':lastname' => $this->lastname));

    echo '<div class="alert alert-success float" role="alert">' .
        "Le Casier a bien était mis a jour." .
        '</div>';

  }

  public function criminalRecordDelete(){
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $req = $dbh->query("DELETE FROM criminal_record WHERE firstname ='$this->firstname' AND lastname ='$this->lastname'");
    echo '<div class="alert alert-success float" role="alert">' .
        "Le casier a bien était supprimé." .
        '</div>';
    return $req;
  }
}
