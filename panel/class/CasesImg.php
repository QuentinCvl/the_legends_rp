<?php


class CasesImg {
  public $casesId;
  public $uploadby;

  public function __construct($casesId, $uploadby) {
    $this->casesId = $casesId;
    $this->uploadby = $uploadby;
  }

  public function addNewImg($src) {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $insert = $dbh->prepare("INSERT INTO `cases-img` (cases_id, src, upload_by ) VALUES (:casesId, :src, :uploadby)");
    $insert->bindParam(':casesId', $this->casesId);
    $insert->bindParam(':src', $src);
    $insert->bindParam(':uploadby', $this->uploadby);
    $insert->execute();

    echo '<div class="alert alert-success float" role="alert">' .
        "Les preuves images ont bien étaient envoyées." .
        '</div>';
  }

}
