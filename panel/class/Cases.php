<?php


class Cases
{
  public $title;
  public $content;

  public function __construct($title, $content) {
    $this->title = $title;
    $this->content = $content;
  }

  public function createCases($createdby, $personnel, $reason) {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $createCases = $dbh->prepare('SELECT title FROM cases WHERE title = :title');
    $createCases->execute(array(':title' => $this->title));
    $nbr = $createCases->rowCount();

    if ($nbr != 0) {
      echo '<div class="alert alert-danger" role="alert">' .
          "Un affaire portant ce titre existe deja, il doit être unique" .
          '</div>';
    } else {
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $sth = $dbh->prepare('INSERT INTO cases(title, charged_personnel, reason, content, created_by) 
            VALUES (:title, :personnel, :reason, :content, :createdby)');
      $sth->bindParam(':title', $this->title);
      $sth->bindParam(':personnel', $personnel);
      $sth->bindParam(':reason', $reason);
      $sth->bindParam(':content', $this->content);
      $sth->bindParam(':createdby', $createdby);

      $sth->execute();

      echo '<div class="alert alert-success float" role="alert">' .
          "Le Casier a bien était créé." .
          '</div>';
    }
  }

  public function updateCases($id, $personel, $reason, $updateby){
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $test = $dbh->prepare('UPDATE cases SET content = :content, title = :title, charged_personnel = :personnel, 
                 reason = :reason, updated_by = :updateby, updated_at = CURRENT_TIMESTAMP() WHERE id = :id');
    $test->execute(array(':content' => $this->content, ':title' => $this->title, ':personnel' => $personel,
        ':reason' => $reason, ':updateby' => $updateby, ':id' => $id));

    echo '<div class="alert alert-success float" role="alert">' .
        "L'affaire a bien était mise a jour." .
        '</div>';
  }

  public function resolveCases($id){
    $status = 'resolve';
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $test = $dbh->prepare('UPDATE cases SET cases_status = :status WHERE id = :id');
    $test->execute(array(':status' => $status, ':id' => $id));

    echo '<div class="alert alert-success float" role="alert">' .
        "L'affaire a bien deplacé dans la rubrique : Résolue." .
        '</div>';
  }

  public function closeCases($id){
    $status = 'close';
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $test = $dbh->prepare('UPDATE cases SET cases_status = :status WHERE id = :id');
    $test->execute(array(':status' => $status, ':id' => $id));

    echo '<div class="alert alert-success float" role="alert">' .
        "L'affaire a bien deplacé dans la rubrique : Classé." .
        '</div>';
  }

  public function ongoingCases($id){
    $status = 'ongoing';
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $test = $dbh->prepare('UPDATE cases SET cases_status = :status WHERE id = :id');
    $test->execute(array(':status' => $status, ':id' => $id));

    echo '<div class="alert alert-success float" role="alert">' .
        "L'affaire a bien deplacé dans la rubrique : En Cours." .
        '</div>';
  }

  public function deleteCases($id){
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $req = $dbh->query("DELETE FROM cases WHERE id ='$id'");
    echo '<div class="alert alert-success float" role="alert">' .
        "Le casier a bien était supprimé." .
        '</div>';
    return $req;
  }
}
