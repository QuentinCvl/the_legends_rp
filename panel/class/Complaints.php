<?php


class Complaints {
  public $describe;
  public $content;

  public function __construct($describe, $content) {
    $this->describe = $describe;
    $this->content = $content;
  }


  public function createComplaint($createdby, $posterFirstname ,$posterLastname, $suspectFirstname, $suspectLastname) {
    $status = "ongoing";
    $deposed_by = $posterFirstname.' '.$posterLastname;
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $sth = $dbh->prepare('INSERT INTO complaints(created_by, deposed_by, firstname, lastname, suspect_description, content, status ) 
            VALUES (:createdby, :deposedby, :firstname, :lastname, :describe, :content, :status )');
    $sth->bindParam(':createdby', $createdby);
    $sth->bindParam(':deposedby', $deposed_by);
    $sth->bindParam(':firstname', $suspectFirstname);
    $sth->bindParam(':lastname', $suspectLastname);
    $sth->bindParam(':describe', $this->describe);
    $sth->bindParam(':content', $this->content);
    $sth->bindParam(':status', $status);
    $sth->execute();

    echo '<div class="alert alert-success float" role="alert">' .
        "La plainte a bien était déposée." .
        '</div>';
  }

  public function updateComplaint($id, $updateby) {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $test = $dbh->prepare('UPDATE complaints SET content = :content, suspect_description = :describe,
                      update_by = :updateby, update_at = CURRENT_TIMESTAMP() WHERE id = :id');
    $test->execute(array(':content' => $this->content, ':describe' => $this->describe, ':updateby' => $updateby, ':id' => $id));

    echo '<div class="alert alert-success float" role="alert">' .
        "La plainte a bien était mise a jour." .
        '</div>';
  }

  public function closeComplaint($id, $closeby, $reason){
    $status = 'close';
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $test = $dbh->prepare('UPDATE complaints SET status = :status, close_by = :closeby, 
                      close_reason = :reason, close_at = CURRENT_TIMESTAMP WHERE id = :id');
    $test->execute(array(':status' => $status, ':closeby' => $closeby, ':reason' => $reason, ':id' => $id));

    echo '<div class="alert alert-success float" role="alert">' .
        "La plainte a bien était classé." .
        '</div>';
  }

  public function deleteComplaint($id){
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $req = $dbh->query("DELETE FROM complaints WHERE id ='$id'");
    echo '<div class="alert alert-success float" role="alert">' .
        "Le plainte a bien était supprimé." .
        '</div>';
    return $req;
  }
}
