<?php


class WantedPoeple {
  public $firstname;
  public $lastname;

  public function __construct($firstname, $lastname) {
    $this->firstname = $firstname;
    $this->lastname = $lastname;
  }

  public function createWantedPoeple($description, $complaint, $createdby) {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $create = $dbh->prepare('SELECT firstname AND lastname FROM `wanted-poeple` 
        WHERE firstname = :firstname AND lastname = :lastname');
    $create->execute(array(':firstname' => $this->firstname, ':lastname' => $this->lastname));
    $nbr = $create->rowCount();

    if ($nbr != 0) {
      echo '<div class="alert alert-danger" role="alert">' .
          "Un avis de recherche pour cette personne existe déja." .
          '</div>';
    } else {
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $sth = $dbh->prepare('INSERT INTO `wanted-poeple`(firstname, lastname, physical_description, complained, created_by) 
            VALUES (:firstname, :lastname, :description, :complained, :createdby)');
      $sth->bindParam(':firstname', $this->firstname);
      $sth->bindParam(':lastname', $this->lastname);
      $sth->bindParam(':description', $description);
      $sth->bindParam(':complained', $complaint);
      $sth->bindParam(':createdby', $createdby);

      $sth->execute();

      echo '<div class="alert alert-success float" role="alert">' .
          "L'avis de recherche a bien était créé." .
          '</div>';
    }
  }

  public function updateWantedPoeple($id, $description, $complaint) {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $test = $dbh->prepare('UPDATE `wanted-poeple` SET firstname = :firstname, lastname = :lastname, 
                           physical_description = :description, complained = :complaint WHERE id = :id');
    $test->execute(array(':firstname' => $this->firstname, ':lastname' => $this->lastname, ':description' => $description,
        ':complaint' => $complaint, ':id' => $id));

    echo '<div class="alert alert-success float" role="alert">' .
        "L'avis de recherche a bien était mise a jour." .
        '</div>';
  }

  public function deleteWantedPoeple($id){
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $req = $dbh->query("DELETE FROM `wanted-poeple` WHERE id ='$id'");
    echo '<div class="alert alert-success float" role="alert">' .
        "L'avis de recherche a bien était supprimé." .
        '</div>';
    return $req;
  }
}
