<?php
if (!isset($_SESSION['grade']) || ($_SESSION['grade'] != "Administrateur" && $_SESSION['grade'] != 'Commandant' && $_SESSION['grade'] != 'Capitaine' && $_SESSION['grade'] != 'Shériff' && $_SESSION['grade'] != 'Shériff-Adjoint' && $_SESSION['grade'] != 'Gouverneur')) {
  echo '<script> document.location.replace("index.php");</script>';
}
?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0 ">Administration</h1>
  </div>

  <!-- Content Row -->
  <div class="row" style="justify-content: space-evenly; margin-bottom: 4%;">
    <a href="#create" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Créer un compte
    </a>
    <a href="#modif" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Modifier / Supprimer un compte
    </a>
  </div>
  <?php
  if (isset($_POST['register_username']) && isset($_POST['register_pswd'])) {
    $new_user = new Users($_POST['register_username'], $_POST['register_pswd']);
    $new_user->register($_POST['register_username'], $_POST['register_pswd'], $_POST['register_firstname'],
        $_POST['register_lastname'], $_POST['register_profession'], $_POST['register_grade'], $_POST['createdby_username']);
  }
  /* Procédure pour mettre a jour les informations de l'utilisateur */
  if (isset($_POST['update_button']) && isset($_POST['update_username'])) {
    if (isset($_POST['update_pswd']) && !empty($_POST['update_pswd'])) {
      $update_user = new Users($_POST['update_username'], $_POST['update_pswd']); // Si mdp modifier
      $update_user->adminUpdate($_POST['update_firstname'], $_POST['update_lastname'], $_POST['update_profession'], $_POST['update_grade'], $_POST['createdby_username']);
    } elseif (isset($_POST['update_pswd']) && empty($_POST['update_pswd'])) {
      $update_user = new Users($_POST['update_username'], $_POST['update_pswd']); // Sinon envoie le mdp présent en base
      $update_user->updateWithoutMdp($_POST['update_firstname'], $_POST['update_lastname'], $_POST['update_profession'], $_POST['update_grade'], $_POST['createdby_username']);
    }
  }
  if (isset($_POST['delete_button']) && isset($_POST['update_username'])) {
    $delete_user = new Users($_POST['update_username'], $_POST['update_pswd']);
    $delete_user->deleteUser();
  }
  ?>
  <hr class="sidebar-divider my-3">
  <!-- Create Account -->
  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="create">
      <h2 class="mb-4">Créer un compte</h2>
      <form method="post" action="index.php?page=administration">
        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="register_username">Username</span>
          </div>
          <input type="text" class="form-control" placeholder="Username" aria-label="Username"
                 aria-describedby="register_username" name="register_username">
          <input type="hidden" name="createdby_username" value="<?php echo $_SESSION['username'] ?>">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="register_pswd">MDP Provisoire</span>
          </div>
          <input type="password" class="form-control" placeholder="Mot de Passe" aria-label="Mot de Passe"
                 aria-describedby="register_pswd" id="input_register_pswd" name="register_pswd" autocomplete>
          <div class="input-group-append">
            <div class="input-group-text">
              <input type="checkbox" aria-label="Show Password" id="checkbox_register_pswd">
            </div>
          </div>
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="register_firstname">Prénom</span>
          </div>
          <input type="text" class="form-control" placeholder="Firstname" aria-label="Firstname"
                 aria-describedby="register_firstname" name="register_firstname">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="register_lastname">Nom</span>
          </div>
          <input type="text" class="form-control" placeholder="Lastname" aria-label="Lastname"
                 aria-describedby="register_lastname" name="register_lastname">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="register_profession">Profession</span>
          </div>
          <select class="custom-select" id="input_register_profession" name="register_profession">
            <option>-- Choississez --</option>
            <option value="lspd">LSPD - Police du Sud</option>
            <option value="lssd">LSSD - Police du Nord</option>
            <option value="gouv">Gouvernement</option>
          </select>
        </div>
        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="register_grade">Grade</span>
          </div>
          <select class="custom-select" id="input_register_grade" name="register_grade">
          </select>
        </div>
        <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm">
            Valider
          </button>
        </div>
      </form>
    </div>

  </div>
  <hr class="sidebar-divider my-3">
  <!-- Create Account -->
  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="modif">
      <h2 class="mb-4">Modifier / Supprimer un compte</h2>
      <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Liste des Utilisateurs</h6>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="users" style="width='100%'; cellspacing='0'">
                <thead> <!-- Apparaitra en haut -->
                <tr>
                  <th class="red">Username</th>
                  <th class="red">Prénom</th>
                  <th class="red">Nom</th>
                  <th class="red">Profession</th>
                  <th class="red">Grade</th>
                  <th class="red">Modifier</th>
                </tr>
                </thead>
                <?php
                $BDD = new BDD();
                $dbh = $BDD->getConnection();
                $stmt = $dbh->query('SELECT * FROM users ');

                echo '<tbody>';
                foreach ($stmt as $row) {
                  if ($row['profession'] != 'admin') {
                    echo '<tr><form method="post" action="index.php?page=update_user" > <!-- Contenu, géré via bdd -->
                          <td>' . $row['username'] . '</td>
                          <td>' . $row['firstname'] . '</td>
                          <td>' . $row['lastname'] . '</td>
                          <td>';
                    if ($row['profession'] == 'lspd') {
                      echo 'LSPD';
                    } else if ($row['profession'] == 'lssd') {
                      echo 'LSSD';
                    } else {
                      echo 'Gouvernement';
                    }
                    echo '</td>
                          <td>' . $row['grade'] . '</td>
                          <input type="hidden" value="' . $row['username'] . '" name="currentUser">
                          <td style="text-align: center">
                            <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm modifUser" name="modifUser"></button>
                           </td>
                        </form></tr>';
                  }
                }
                echo '</tbody>';
                ?>
              </table>
            </div>
          </div>
        </div>


      </div>
      <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

  </div>
</div>
<div class="confirm_popup" id="createAccounCheck">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Félicitation, le compte a bien était créé !</div>
      <div class="modal-footer">
        <a href="index.php?page=administration">
          <button class="btn btn-success" type="button" data-dismiss="modal">Fermer</button>
        </a>
      </div>
    </div>
  </div>
</div>
</div>
<!-- End of Main Content -->


<script>
    $(document).ready(function () {
        var checkbox = document.getElementById('checkbox_register_pswd');
        checkbox.addEventListener('change', function () {
            var input = document.getElementById('input_register_pswd');

            if (this.checked) {
                input.setAttribute("type", "text");
            } else {
                input.setAttribute("type", "password");
            }

        })


        $('#input_register_profession').on('change', function () {
            var selectedValue = $(this).val();
            var option = document.getElementById('input_register_grade');
            var lspdArray = ["Recrue", "Cadet", "Officier", "Sergent", "Lieutenant", "Capitaine", "Commandant"];
            var lssdArray = ["Recrue", "Aspirant Ranger", "Ranger", "Sergent-Chef", "Shériff-Adjoint", "Shériff"];
            var gouvArray = ["Juge", "Procureur", "Agent de sécurité", "Chef de la Sécurité", "Conseiller de l'état", "Gouverneur"];

            if (selectedValue === 'lspd') {
                var length = option.options.length;
                for (i = length - 1; i >= 0; i--) {
                    option.options[i] = null;
                }

                $.each(lspdArray, function (key, value) {
                    var newoption = document.createElement('option');
                    newoption.innerHTML = value;
                    newoption.value = value;
                    option.appendChild(newoption);
                })
            } else if (selectedValue === 'lssd') {
                var length = option.options.length;
                for (i = length - 1; i >= 0; i--) {
                    option.options[i] = null;
                }
                $.each(lssdArray, function (key, value) {
                    var newoption = document.createElement('option');
                    newoption.innerHTML = value;
                    newoption.value = value;
                    option.appendChild(newoption);
                })
            } else if (selectedValue === 'gouv') {
                var length = option.options.length;
                for (i = length - 1; i >= 0; i--) {
                    option.options[i] = null;
                }
                $.each(gouvArray, function (key, value) {
                    var newoption = document.createElement('option');
                    newoption.innerHTML = value;
                    newoption.value = value;
                    option.appendChild(newoption);
                })
            } else {
                var length = option.options.length;
                for (i = length - 1; i >= 0; i--) {
                    option.options[i] = null;
                }
            }
        })
        $('.modifUser').on('click', function () {
            var updateform = document.getElementById('modifActive');
            updateform.style.display = "block";
        })
        $('.close_update').on('click', function () {
            var updateform = document.getElementById('modifActive');
            updateform.style.display = "none";
        })
    })
</script>
