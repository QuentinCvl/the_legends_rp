<?php
if (isset($_POST['showCases'])) {
  $BDD = new BDD();
  $dbh = $BDD->getConnection();
  $req = $dbh->prepare('SELECT * FROM cases WHERE id = ?');
  $req->execute(array($_POST['currentId']));
  $req = $req->fetch(PDO::FETCH_ASSOC);
}
?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Affaire : <?php echo $req['title'] ?></h1>
  </div>
  <p>Casier N° <?php echo $req['id'] ?></p>
  <p>Créé par <?php echo $req['created_by'] ?></p>
  <p>Le <?php echo $req['created_at'] ?></p>
  <?php if (!empty($req['updated_at'])) {
    echo '<p>Modifié en dernier le : ' . $req['updated_at'] . "</p>";
    echo '<p>Par : ' . $req['updated_by'] . "</p>";
  }
  ?>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Chargé de l'affaire</h2>
      <div class="container-fluid">
        <?php echo nl2br($req['charged_personnel']) ?>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Objet de l'affaire</h2>
      <div class="container-fluid">
        <?php echo nl2br($req['reason']) ?>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Contenu de l'affaire</h2>
      <div class="container-fluid">
        <?php echo nl2br($req['content']) ?>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
    <form method="post" action="index.php?page=resolved-cases">
      <input type="hidden" value="<?php echo $req['title'] ?>" name="update_cases_title">
      <input type="hidden" value="<?php echo $req['content'] ?>" name="update_cases_content">
      <input type="hidden" value="<?php echo $req['id'] ?>" name="update_cases_id">
      <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"
              name="ongoing-cases-button">
        En cours
      </button>
      <?php
      if ($_SESSION['grade'] == "Administrateur" || $_SESSION['grade'] == 'Commandant' || $_SESSION['grade'] == 'Capitaine' || $_SESSION['grade'] == 'Shériff' || $_SESSION['grade'] == 'Shériff-Adjoint' || $_SESSION['grade'] == 'Gouverneur') {
        echo '
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" name="delete-cases-button">
            Supprimer
          </button>
        </div>';
      }
      ?>
    </form>
  </div>

</div>

<script>

    $('#showUpdateCriminalRecord').on('click', function () {
        var show = document.getElementById('updateCriminalRecord');
        show.style.display = "block";
        var hide = document.getElementById('showUpdateCriminalRecord');
        hide.className = "d-none-btn";
    })
</script>
