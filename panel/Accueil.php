<?php
$BDD = new BDD();
$dbh = $BDD->getConnection();
$stmt = $dbh->query("SELECT * FROM cases");
$nbr = $stmt->rowCount();
if ($nbr != 0) {
  $ongoing = 0;
  $resolve = 0;
  $close = 0;
  foreach ($stmt as $row) {
    if ($row['cases_status'] == "ongoing") {
      $ongoing = $ongoing + 1;
    } elseif ($row['cases_status'] == "resolve") {
      $resolve = $resolve + 1;
    } elseif ($row['cases_status'] == "close") {
      $close = $close + 1;
    }
  }
}


?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Affaires</h1>
    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      <i class="fas fa-search fa-sm text-white-50"></i> Nouvelle Affaire
    </a>
  </div>

  <!-- Content Row -->
  <div class="row">

    <!-- Earnings (Monthly) Card Example -->

    <div class="col-xl-3 col-md-6 mb-4">
      <a href="index.php?page=ongoing-cases" class="noStyleLink">
        <div class="card border-left-primary shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">En Cours</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                  <?php if (isset($ongoing)) echo $ongoing; else echo '0' ?>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-folder-open fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>


    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
      <a href="index.php?page=resolved-cases" class="noStyleLink">
        <div class="card border-left-success shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Résolues</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                  <?php if (isset($resolve)) echo $resolve; else echo '0' ?>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-check fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>

    <!-- Pending Requests Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
      <a href="index.php?page=closed-cases" class="noStyleLink">
        <div class="card border-left-warning shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Classé</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                  <?php if (isset($close)) echo $close; else echo '0' ?>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-folder fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-info text-uppercase mb-1">% de réussite</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                    <?php if (isset($resolve) && $resolve != 0) {
                      $pourcent = ($close * 100) / $resolve;
                      $pourcent = round($pourcent, 2, PHP_ROUND_HALF_UP);
                      $pourcent = 100 - $pourcent;
                      echo $pourcent;
                    } else echo '0' ?> %
                  </div>
                </div>
                <div class="col">
                  <div class="progress progress-sm mr-2">
                    <div class="progress-bar bg-info" role="progressbar" style="width: <?php echo $pourcent ?>%"
                         aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-percent fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>


</div>
<!-- End of Main Content -->


