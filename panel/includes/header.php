<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Legends - Dashboard</title>

  <link rel="icon" href="../favicon.ico">

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="includes/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

  <!-- Sidebar -->
  <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
      <div class="sidebar-brand-icon">
        <img src="includes/img/logo.png" style="width: 40px; height: 40px;">
      </div>
      <div class="sidebar-brand-text mx-3">Los Santos<br/>Department</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
      <a class="nav-link" href="index.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Tableau de bord</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      Citoyen
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <!--<li class="nav-item">
      <a class="nav-link collapsed" href="index.php?page=recensement">
        <i class="fas fa-user-check fa-cog"></i>
        <span>Recensement</span>
      </a>
    </li>-->
    <li class="nav-item">
      <a class="nav-link collapsed" href="index.php?page=criminal-record">
        <i class="fas fa-folder fa-cog"></i>
        <span>Casier judiciaire</span>
      </a>
    </li>
    <!--<li class="nav-item">
      <a class="nav-link collapsed" href="index.php?page=weapon-license">
        <i class="fas fa-clipboard "></i>
        <span>Permis port d'armes</span>
      </a>
    </li>-->
    <!--<li class="nav-item">
      <a class="nav-link collapsed" href="index.php?page=vehicules-identified">
        <i class="fas fa-car "></i>
        <span>Véhicules recensés</span>
      </a>
    </li>-->

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      Dossier
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
         aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-fw fa-folder"></i>
        <span>Rapport</span>
      </a>
      <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Rapport :</h6>
          <a class="collapse-item" href="index.php?page=ongoing-cases">En cours</a>
          <a class="collapse-item" href="index.php?page=resolved-cases">Résolues</a>
          <a class="collapse-item" href="index.php?page=closed-cases">Classé</a>
        </div>
      </div>
    </li>

    <!-- Nav Item - Charts -->
    <li class="nav-item">
      <a class="nav-link" href="index.php?page=complaints">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Plaintes</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages2"
         aria-expanded="true" aria-controls="collapsePages2">
        <i class="fas fa-fw fa-folder"></i>
        <span>Avis de recherche</span>
      </a>
      <div id="collapsePages2" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Recherche de :</h6>
          <a class="collapse-item" href="index.php?page=wanted-poeple">Personne</a>
          <a class="collapse-item" href="index.php?page=wanted-vehicles">Véhicules</a>
        </div>
      </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>
  <!-- End of Sidebar -->

  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

      <!-- Topbar -->
      <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
          <i class="fa fa-bars"></i>
        </button>

        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

          <!-- Nav Item - User Information -->
          <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="mr-2 d-lg-inline text-gray-600 small">
                <?php if (isset($_SESSION['username'])) echo $_SESSION['grade'] . ' ' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname'] ?>
              </span>
              <img class="img-profile rounded-circle"
                   src="includes/img/<?php if (isset($_SESSION['profession'])) {
                     if ($_SESSION['profession'] == "lspd") echo "lspd.png";
                     if ($_SESSION['profession'] == "lssd") echo "lssd.png";
                     if ($_SESSION['profession'] == "gouv") echo "logo.png";
                     if ($_SESSION['profession'] == "admin") echo "legends_logo.png";
                   } ?>">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                 aria-labelledby="userDropdown">
              <a class="dropdown-item" href="index.php?page=profil">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Profil
              </a>
              <?php if ($_SESSION['grade'] == "Administrateur" || $_SESSION['grade'] == 'Commandant' || $_SESSION['grade'] == 'Capitaine' || $_SESSION['grade'] == 'Shériff' || $_SESSION['grade'] == 'Shériff-Adjoint' || $_SESSION['grade'] == 'Gouverneur') {
                echo '<a class="dropdown-item" href="index.php?page=administration">
                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                Administration
              </a>';
              } ?>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="disconnect.php" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Déconnexion
              </a>
            </div>
          </li>

        </ul>

      </nav>
      <!-- End of Topbar -->
      <!-- JQuery CDN -->
      <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
