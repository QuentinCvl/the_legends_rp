<?php
/* Récupére les données de l'utilisateur choisie pour remplir les champs avec les valeurs le concernant */
if (isset($_POST['currentUser'])) {
  $BDD = new BDD();
  $dbh = $BDD->getConnection();
  $req = $dbh->prepare('SELECT * FROM users WHERE username = ?');
  $req->execute(array($_POST['currentUser']));
  $req = $req->fetch(PDO::FETCH_ASSOC);
}

?>
<div class="update_bg" id="modifActive">
  <div class="close_update"><a href="index.php?page=administration">&cross;</a></div>
  <div class="update_info">
    <h2>Mise à jour / Suppréssion Utilisateur : <?php if (isset($req['username'])) echo $req['username']; ?></h2>
    <p>L'username ne peux être modifié, passé par un admin pour modifier celui-ci.
      Pour le reste, laissez tel quel ce qui ne doit être modifier, et laisser le champs mdp vide
      si le mot de passe ne doit pas être modifié</p>
  </div>
  <div class="update_popup">
    <form method="post" action="index.php?page=administration">
      <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
        <div class="input-group-prepend">
          <span class="input-group-text" id="update_username">Username</span>
        </div>
        <input type="text" class="form-control" placeholder="Username" aria-label="Username"
               aria-describedby="update_username" name="Affiche_username" disabled
               value="<?php if (isset($req['username'])) echo $req['username']; ?>">
        <input type="hidden" name="update_username"
               value="<?php if (isset($req['username'])) echo $req['username']; ?>">
        <input type="hidden" name="createdby_username" value="<?php echo $_SESSION['username']?>">
      </div>

      <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
        <div class="input-group-prepend">
          <span class="input-group-text" id="update_pswd">Nouveau MDP</span>
        </div>
        <input type="password" class="form-control" placeholder="Mot de Passe" aria-label="Mot de Passe"
               aria-describedby="update_pswd" id="input_update_pswd" name="update_pswd">
        <div class="input-group-append">
          <div class="input-group-text">
            <input type="checkbox" aria-label="Show Password" id="checkbox_update_pswd">
          </div>
        </div>
      </div>

      <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
        <div class="input-group-prepend">
          <span class="input-group-text" id="update_firstname">Prénom</span>
        </div>
        <input type="text" class="form-control" placeholder="Firstname" aria-label="Firstname"
               aria-describedby="update_firstname" name="update_firstname"
               value="<?php if (isset($req['firstname'])) echo $req['firstname']; ?>">
      </div>

      <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
        <div class="input-group-prepend">
          <span class="input-group-text" id="update_lastname">Nom</span>
        </div>
        <input type="text" class="form-control" placeholder="Lastname" aria-label="Lastname"
               aria-describedby="update_lastname" name="update_lastname"
               value="<?php if (isset($req['lastname'])) echo $req['lastname']; ?>">
      </div>

      <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
        <div class="input-group-prepend">
          <span class="input-group-text" id="update_profession">Profession</span>
        </div>
        <select class="custom-select" id="input_update_profession" name="update_profession">
          <option>-- Choississez --</option>
          <option value="lspd">LSPD - Police du Sud</option>
          <option value="lssd">LSSD - Police du Nord</option>
          <option value="gouv">Gouvernement</option>
        </select>
      </div>
      <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
        <div class="input-group-prepend">
          <span class="input-group-text" id="update_grade">Grade</span>
        </div>
        <select class="custom-select" id="input_update_grade" name="update_grade">
        </select>
      </div>
      <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
        <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm" name="update_button">
          Valider
        </button>
        <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" name="delete_button">
          Supprimer
        </button>
      </div>
    </form>
  </div>
</div>

<script>
    $(document).ready(function () {
        var checkbox = document.getElementById('checkbox_update_pswd');
        checkbox.addEventListener('change', function () {
            var input = document.getElementById('input_update_pswd');

            if (this.checked) {
                input.setAttribute("type", "text");
            } else {
                input.setAttribute("type", "password");
            }

        })


        $('#input_update_profession').on('change', function () {
            var selectedValue = $(this).val();
            var option = document.getElementById('input_update_grade');
            var lspdArray = ["Recrue", "Cadet", "Officier", "Sergent", "Capitaine", "Commandant"];
            var lssdArray = ["Recrue", "Aspirant Ranger", "Ranger", "Sergent-Chef", "Shériff-Adjoint", "Shériff"];
            var gouvArray = ["Juge", "Procureur", "Agent de sécurité", "Chef de la Sécurité", "Conseiller de l'état", "Gouverneur"];

            if (selectedValue === 'lspd') {
                var length = option.options.length;
                for (i = length - 1; i >= 0; i--) {
                    option.options[i] = null;
                }

                $.each(lspdArray, function (key, value) {
                    var newoption = document.createElement('option');
                    newoption.innerHTML = value;
                    newoption.value = value;
                    option.appendChild(newoption);
                })
            } else if (selectedValue === 'lssd') {
                var length = option.options.length;
                for (i = length - 1; i >= 0; i--) {
                    option.options[i] = null;
                }
                $.each(lssdArray, function (key, value) {
                    var newoption = document.createElement('option');
                    newoption.innerHTML = value;
                    newoption.value = value;
                    option.appendChild(newoption);
                })
            } else if (selectedValue === 'gouv') {
                var length = option.options.length;
                for (i = length - 1; i >= 0; i--) {
                    option.options[i] = null;
                }
                $.each(gouvArray, function (key, value) {
                    var newoption = document.createElement('option');
                    newoption.innerHTML = value;
                    newoption.value = value;
                    option.appendChild(newoption);
                })
            } else {
                var length = option.options.length;
                for (i = length - 1; i >= 0; i--) {
                    option.options[i] = null;
                }
            }
        })
    })
</script>
