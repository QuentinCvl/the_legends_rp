<?php
include_once("autoload.php");

if (isset($_SESSION["username"])) {
  include_once("includes/header.php");

  if (isset ($_GET['page']) && file_exists($_GET['page'] . '.php')) {
    require_once($_GET['page'] . '.php');
  } elseif (!isset($_GET['page'])) {
    require_once("Accueil.php");
  } else {
    require_once("404.html");
  }

  include_once("includes/footer.php");
}
else {
  include("includes/register_header.php");

  if(isset ($_GET['page']) && file_exists($_GET['page'].'.php')) {
    require($_GET['page'].'.php');
  } else {
    require("register.php");
  }

  include("includes/register_footer.html");

}

