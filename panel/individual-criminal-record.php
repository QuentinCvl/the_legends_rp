<?php
if (isset($_POST['currentId'])) {
  $BDD = new BDD();
  $dbh = $BDD->getConnection();
  $req = $dbh->prepare('SELECT * FROM criminal_record WHERE id = ?');
  $req->execute(array($_POST['currentId']));
  $req = $req->fetch(PDO::FETCH_ASSOC);
}
?>
<!-- Begin Page Content -->
<div class="container-fluid" style="position: relative">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0 ">Casier Judiciaire de <?php echo $req['firstname'] . ' ' . $req['lastname'] ?></h1>
  </div>
  <p>Casier N° <?php echo $req['id'] ?></p>
  <p>Créé par : <?php echo $req['created_by'] ?></p>
  <p>Le : <?php echo $req['created_at'] ?></p>
  <?php if (!empty($req['update_at'])) {
  echo '<p>Modifié en dernier le : '.$req['update_at']."</p>";
  echo '<p>Par : '.$req['update_by']."</p>";
  }
  ?>
  <div class="criminal-record-img">
    <img src="<?php echo $req['img']?>">
  </div>
  <hr class="sidebar-divider my-3" style="width: 50%">

  <p>Nom du criminel : <?php echo $req['firstname'] . ' ' . $req['lastname'] ?></p>
  <p>Date de naissance : <?php echo $req['birthday'] ?></p>
  <p>Adresse connu : <?php echo $req['adress'] ?></p>
  <p>Organisation : <?php echo $req['organisation'] ?></p>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Faits repprochés</h2>
      <div class="container-fluid">
        <?php echo nl2br($req['condamnation']) ?>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Peines encourues</h2>
      <div class="container-fluid">
        <?php echo nl2br($req['sentence']) ?>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
    <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm" id="showUpdateCriminalRecord">
      Modifier
    </button>
  </div>

  <div class="row" style="display: none" id="updateCriminalRecord">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Modifier le casier</h2>
      <form method="post" action="index.php?page=criminal-record">
        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="update_criminal_adress">Adresse</span>
          </div>
          <input type="text" class="form-control" aria-label="Adress" required value="<?php echo $req['adress'] ?>"
                 aria-describedby="update_criminal_adress" name="update_criminal_adress">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="update_criminal_organisation">Organisation</span>
          </div>
          <input type="text" class="form-control"aria-label="Organisation" required value="<?php echo $req['organisation'] ?>"
                 aria-describedby="update_criminal_organisation" name="update_criminal_organisation">
        </div>
        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Condamnation</span>
          </div>
          <textarea maxlength="65000" required class="form-control" aria-label="content" name="update_criminal_condamnation" style="min-height: 300px;"><?php
            echo $req['condamnation'];
            ?></textarea>
          <input type="hidden" value="<?php echo $req['firstname'] ?>" name="update-criminal-firstname">
          <input type="hidden" value="<?php echo $req['lastname'] ?>" name="update-criminal-lastname">
          <input type="hidden" name="update_criminal_updateby"
                 value="<?php echo $_SESSION['grade'] . ' ' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname'] ?>">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Peines<br>encourues</span>
          </div>
          <textarea maxlength="65000" required class="form-control" aria-label="content" name="update_criminal_sentence" style="min-height: 300px;"><?php
            echo $req['sentence'];
            ?></textarea>
        </div>
        <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm" name="update-criminal-record-button">
            Valider
          </button>
        </div>
        <?php
        if($_SESSION['grade'] == "Administrateur" || $_SESSION['grade'] == 'Commandant' || $_SESSION['grade'] == 'Capitaine' || $_SESSION['grade'] == 'Shériff' || $_SESSION['grade'] == 'Shériff-Adjoint' || $_SESSION['grade'] == 'Gouverneur') {
          echo '<div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" name="delete-criminal-record-button">
            Supprimer
          </button>
        </div>';
        }
        ?>
      </form>
    </div>
  </div>

</div>

<script>

  $('#showUpdateCriminalRecord').on('click', function () {
      var show = document.getElementById('updateCriminalRecord');
      show.style.display= "block";
      var hide = document.getElementById('showUpdateCriminalRecord');
      hide.className = "d-none";
  })
</script>
