<?php
if(!isset($_SESSION['grade'])) { // Si la personne n'est plus connecté, elle est redirigé vers la page de connexion
  echo '<script> document.location.replace("index.php");</script>';
}
if (isset($_POST['showCases'])) {
  $BDD = new BDD();
  $dbh = $BDD->getConnection();
  $req = $dbh->prepare('SELECT * FROM cases WHERE id = ?');
  $req->execute(array($_POST['currentId']));
  $req = $req->fetch(PDO::FETCH_ASSOC);
}
?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Affaire : <?php echo $req['title'] ?></h1>
  </div>
  <p>Casier N° <?php echo $req['id'] ?></p>
  <p>Créé par <?php echo $req['created_by'] ?></p>
  <p>Le <?php echo $req['created_at'] ?></p>
  <?php if (!empty($req['updated_at'])) {
    echo '<p>Modifié en dernier le : ' . $req['updated_at'] . "</p>";
    echo '<p>Par : ' . $req['updated_by'] . "</p>";
  }
  ?>
  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Chargé de l'affaire</h2>
      <div class="container-fluid">
        <?php echo nl2br($req['charged_personnel']) ?>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Objet de l'affaire</h2>
      <div class="container-fluid">
        <?php echo nl2br($req['reason']) ?>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Détails</h2>
      <div class="container-fluid">
        <?php echo nl2br($req['content']) ?>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Preuves photos</h2>
      <?php
      $casesid = $req['id'];
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $stmt = $dbh->query("SELECT * FROM `cases-img` WHERE cases_id = '$casesid'");

      ?>
      <div class="container-fluid cases_img_flex">
        <?php
        foreach ($stmt as $row) {
          echo '<div class="cases_img_container">
                  <form action="index.php?page=ongoing-cases" method="post">
                    <input type="hidden" name="delete-src" value="'.$row['src'].'">
                    <button type="submit" name="delete-img" value="'.$row["id"].'" class="top-right">
                     <i class="fas fa-trash-alt"></i>
                    </button>
                  </form>
                  <a href="' . $row['src'] . '"><img src=' . $row['src'] . '></a>
              </div>';
        }
        ?>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Ajouter des preuves (photos)</h2>
      <div class="container-fluid">
        <div class="col-xl-10 col-md-10 mb-10">
          <h2 style="font-size: 0.8rem">
            Essayez de limiter à max 5 photos (png / jpg / jpeg).<br>
            Renommez vos fichiers avant de les uploads car il prendront le même nom.<br>
            Appellez les par exemple comme ceci : titredel'affaire-drogue.png
          </h2>
          <form action="index.php?page=ongoing-cases" method="post" enctype="multipart/form-data">
            <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
              <div class="input-group-prepend">
                <span class="input-group-text">Images</span>
              </div>
              <input type="file" class="form-control" name="cases_img[]" multiple
                     accept=".png, .jpeg, .jpg" max="5">
              <input type="hidden" value="<?php echo $req['id'] ?>" name="cases-id">
              <input type="hidden"
                     value="<?php echo $_SESSION['grade'] . ' ' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname'] ?>"
                     name="update_by">
              <input type="hidden" name="cases-title" value="<?php echo $req['title'] ?>">
            </div>

            <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
              <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm"
                      name="cases-add-img">Ajouter
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <hr class="sidebar-divider my-3">

  <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
    <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm"
            id="showUpdateCriminalRecord">
      Modifier
    </button>
  </div>

  <div class="row" style="display: none" id="updateCriminalRecord">
    <div class="col-xl-12 col-md-12 mb-12">
      <h2 class="mb-4">Modifier l'affaire en cours</h2>
      <form method="post" action="index.php?page=ongoing-cases">
        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text" id="update_cases_title">Titre</span>
          </div>
          <input type="text" class="form-control" placeholder="Titre" aria-label="title" required
                 aria-describedby="update_cases_title" name="update_cases_title"
                 value="<?php echo $req['title'] ?>">
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Chargé de l'affaire</span>
          </div>
          <textarea maxlength="65000" required class="form-control" aria-label="update_charged_personnel"
                    name="update_charged_personnel"><?php
            echo $req['charged_personnel'];
            ?></textarea>
        </div>

        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Objet de l'affaire</span>
          </div>
          <textarea maxlength="65000" required class="form-control" aria-label="reason" name="update_cases_reason"><?php
            echo $req['reason'];
            ?></textarea>
        </div>
        <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
          <div class="input-group-prepend">
            <span class="input-group-text">Contenu</span>
          </div>
          <textarea maxlength="65000" required class="form-control" aria-label="content" name="update_cases_content"
                    style="min-height: 300px;"><?php
            echo $req['content'];
            ?></textarea>
          <input type="hidden" value="<?php echo $req['id'] ?>" name="update_cases_id">
          <input type="hidden"
                 value="<?php echo $_SESSION['grade'] . ' ' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname'] ?>"
                 name="update_by">
        </div>
        <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm"
                  name="update-cases-button">
            Valider
          </button>
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"
                  name="resolve-cases-button">
            Résolue
          </button>
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-warning shadow-sm"
                  name="close-cases-button">
            Classé
          </button>
        </div>
        <?php
        if ($_SESSION['grade'] == "Administrateur" || $_SESSION['grade'] == 'Commandant' || $_SESSION['grade'] == 'Capitaine' || $_SESSION['grade'] == 'Shériff' || $_SESSION['grade'] == 'Shériff-Adjoint' || $_SESSION['grade'] == 'Gouverneur') {
          echo '<div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" name="delete-cases-button">
            Supprimer
          </button>
        </div>';
        }
        ?>
    </div>

    </form>
  </div>
</div>

</div>

<script>

    $('#showUpdateCriminalRecord').on('click', function () {
        var show = document.getElementById('updateCriminalRecord');
        show.style.display = "block";
        var hide = document.getElementById('showUpdateCriminalRecord');
        hide.className = "d-none-btn";
    })
</script>
