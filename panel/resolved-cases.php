<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Affaire Résolues</h1>
  </div>

  <hr class="sidebar-divider my-3">

  <?php
  if (isset($_POST['ongoing-cases-button'])) {
    $update_cases = new Cases($_POST['update_cases_title'], $_POST['update_cases_content']);
    $update_cases->ongoingCases($_POST['update_cases_id']);
  }
  if (isset($_POST['close-cases-button'])) {
    $update_cases = new Cases($_POST['update_cases_title'], $_POST['update_cases_content']);
    $update_cases->closeCases($_POST['update_cases_id']);
  }
  if (isset($_POST['delete-cases-button'])) {
    $delete_cases = new Cases($_POST['update_cases_title'], $_POST['update_cases_content']);
    $delete_cases->deleteCases($_POST['update_cases_id']);
  }
  ?>

  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="showCases">
      <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Liste des affaires résolues</h6>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="users" style="width='100%'; cellspacing='0'">
                <thead> <!-- Apparaitra en haut -->
                <tr>
                  <th class="red">N°</th>
                  <th class="red">Auteur</th>
                  <th class="red">Titre</th>
                  <th class="red">Créé le</th>
                  <th class="red">Voir</th>
                </tr>
                </thead>
                <?php
                $status = "resolve";
                $BDD = new BDD();
                $dbh = $BDD->getConnection();
                $stmt = $dbh->query("SELECT * FROM cases WHERE cases_status = '$status'");

                echo '<tbody>';
                foreach ($stmt as $row) {
                  echo '<tr><form method="post" action="index.php?page=show-resolve-cases" > <!-- Contenu, géré via bdd -->
                          <td>' . $row['id'] . '</td>
                          <td>' . $row['created_by'] . '</td>
                          <td>' . $row['title'] . '</td>
                          <td>' . $row['created_at'] . '</td>
                          <input type="hidden" value="' . $row['id'] . '" name="currentId">
                          <td style="text-align: center">
                            <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm modifUser" name="showCases"></button>
                          </td>
                        </form></tr>';
                }

                echo '</tbody>';
                ?>
              </table>
            </div>
          </div>
        </div>


      </div>
      <!-- /.container-fluid -->
