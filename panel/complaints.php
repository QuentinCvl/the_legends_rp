<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Plaintes déposées</h1>
  </div>

  <!-- Content Row -->
  <div class="row" style="justify-content: space-evenly; margin-bottom: 4%;">
    <a href="#showCases" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Voir les Plaintes
    </a>
    <a href="#newCases" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Créer une nouvelle Affaire
    </a>
    <a href="index.php?page=closed-complaints" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
      Voir les plaintes archivées
    </a>
  </div>

  <hr class="sidebar-divider my-3">

  <?php
  if (isset($_POST['complaint_poster_firstname'])) {
    $new_cases = new Complaints($_POST['complaint_describe'], $_POST['complaint_content']);
    $new_cases->createComplaint($_POST['createdby'], $_POST['complaint_poster_firstname'], $_POST['complaint_poster_lastname'],
        $_POST['complaint_suspect_firstname'], $_POST['complaint_suspect_lastname']);
  }
  if (isset($_POST['update-complaint-button'])) {
    $update_cases = new Complaints($_POST['update_suspect_description'], $_POST['update_content']);
    $update_cases->updateComplaint($_POST['update_complaint_id'], $_POST['update_by']);
  }
  if (isset($_POST['close-complaint-button'])) {
    $update_cases = new Complaints($_POST['update_suspect_description'], $_POST['update_content']);
    $update_cases->closeComplaint($_POST['update_complaint_id'], $_POST['update_by'], $_POST['close_reason']);
  }
  if (isset($_POST['delete-complaint-button'])) {
    $delete_cases = new Complaints($_POST['update_suspect_description'], $_POST['update_content']);
    $delete_cases->deleteComplaint($_POST['update_complaint_id']);
  }
  ?>
  <!-- Show record -->
  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12" id="showCases">
      <h2 class="mb-4">Plaintes</h2>
      <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Liste des plaintes déposées</h6>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="users" style="width='100%'; cellspacing='0'">
                <thead> <!-- Apparaitra en haut -->
                <tr>
                  <th class="red">N°</th>
                  <th class="red">Auteur</th>
                  <th class="red">Nom Déposeur</th>
                  <th class="red">Nom Suspect</th>
                  <th class="red">Créé le</th>
                  <th class="red">Voir</th>
                </tr>
                </thead>
                <?php
                $status = "ongoing";
                $BDD = new BDD();
                $dbh = $BDD->getConnection();
                $stmt = $dbh->query("SELECT * FROM complaints WHERE status = '$status'");

                echo '<tbody>';
                foreach ($stmt as $row) {
                  echo '<tr><form method="post" action="index.php?page=show-complaint" > <!-- Contenu, géré via bdd -->
                          <td>' . $row['id'] . '</td>
                          <td>' . $row['created_by'] . '</td>
                          <td>' . $row['deposed_by'] . '</td>
                          <td>' . $row['firstname'].' '. $row['lastname'] . '</td>
                          <td>' . $row['created_at'] . '</td>
                          <input type="hidden" value="' . $row['id'] . '" name="currentId">
                          <td style="text-align: center">
                            <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm modifUser" name="showComplaint"></button>
                          </td>
                        </form></tr>';
                }

                echo '</tbody>';
                ?>
              </table>
            </div>
          </div>
        </div>

        <hr class="sidebar-divider my-3">

        <div class="col-xl-12 col-md-12 mb-12" id="newCases">
          <h2 class="mb-4">Créer une nouvelle plainte</h2>
          <form method="post" action="index.php?page=complaints">
            <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
              <div class="input-group-prepend">
                <span class="input-group-text" id="complaint_poster_firstname">Prénom du déposeur</span>
              </div>
              <input type="text" class="form-control" placeholder="Prénom" aria-label="firstname" required
                     aria-describedby="complaint_poster_firstname" name="complaint_poster_firstname">
              <input type="hidden" name="createdby"
                     value="<?php echo $_SESSION['grade'] . ' ' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname'] ?>">
            </div>

            <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
              <div class="input-group-prepend">
                <span class="input-group-text" id="complaint_poster_lastname">Nom du déposeur</span>
              </div>
              <input type="text" class="form-control" placeholder="Nom" aria-label="lastname" required
                     aria-describedby="complaint_poster_lastname" name="complaint_poster_lastname">
            </div>

            <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
              <div class="input-group-prepend">
                <span class="input-group-text" id="complaint_suspect_firstname">Prénom du suspect</span>
              </div>
              <input type="text" class="form-control" placeholder="Prénom" aria-label="firstname" required
                     aria-describedby="complaint_suspect_firstname" name="complaint_suspect_firstname">
            </div>

            <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
              <div class="input-group-prepend">
                <span class="input-group-text" id="complaint_suspect_lastname">Nom du suspect</span>
              </div>
              <input type="text" class="form-control" placeholder="Nom" aria-label="lastname" required
                     aria-describedby="complaint_suspect_lastname" name="complaint_suspect_lastname">
            </div>

            <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
              <div class="input-group-prepend">
                <span class="input-group-text">Description<br>du suspect</span>
              </div>
              <textarea maxlength="65000" required class="form-control" aria-label="description" name="complaint_describe"
                        placeholder="Ecrivez ici la description du suspect, tous détails permettant de le reconnaitre"></textarea>
            </div>

            <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
              <div class="input-group-prepend">
                <span class="input-group-text">Contenu</span>
              </div>
              <textarea maxlength="65000" required class="form-control" aria-label="content" name="complaint_content"
                        placeholder="Ecrivez ici"></textarea>
            </div>

            <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
              <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm">
                Valider
              </button>
            </div>
          </form>
        </div>

      </div>
      <!-- /.container-fluid -->
