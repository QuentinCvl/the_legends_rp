<?php
if (isset($_POST['showClosedComplaint'])) {
  $BDD = new BDD();
  $dbh = $BDD->getConnection();
  $req = $dbh->prepare('SELECT * FROM complaints WHERE id = ?');
  $req->execute(array($_POST['currentId']));
  $req = $req->fetch(PDO::FETCH_ASSOC);
}
?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h1 mb-0">Plainte N° <?php echo $req['id'] ?></h1>
  </div>
  <p>Créé par <?php echo $req['created_by'] ?></p>
  <p>Le <?php echo $req['created_at'] ?></p>
  <?php if (!empty($req['update_at'])) {
    echo '<p>Modifié en dernier le : ' . $req['update_at'] . "</p>";
    echo '<p>Par : ' . $req['update_by'] . "</p>";
  }
  ?>
  <hr class="sidebar-divider my-3">

  <p>Indentité du déposeur : <?php echo $req['deposed_by'] ?></p>
  <p>Indentité du présumé suspect : <?php echo $req['firstname'] . ' ' . $req['lastname'] ?></p>

  <hr class="sidebar-divider my-3">


  <div class="col-xl-12 col-md-12 mb-12">
    <h2 class="mb-4">Descriptif du présumé suspect</h2>
    <div class="container-fluid">
      <?php echo nl2br($req['suspect_description']) ?>
    </div>
  </div>


  <hr class="sidebar-divider my-3">


  <div class="col-xl-12 col-md-12 mb-12">
    <h2 class="mb-4">Détails</h2>
    <div class="container-fluid">
      <?php echo nl2br($req['content']) ?>
    </div>
  </div>


  <hr class="sidebar-divider my-3">


  <div class="col-xl-12 col-md-12 mb-12">
    <h2 class="mb-4">Information sur l'archivage de la plainte</h2>
    <p>Archivé par <?php echo $req['close_by'] ?></p>
    <p>Le : <?php echo $req['close_at'] ?></p>
    <p>Raison : </p>
    <div class="container-fluid">
      <?php echo nl2br($req['close_reason']) ?>
    </div>
  </div>


  <div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
      <form method="post" action="index.php?page=closed-complaints">
        <input type="hidden" value="<?php echo $req['id'] ?>" name="delete_complaint_id">
        <input type="hidden" value="<?php echo $req['suspect_description'] ?>" name="delete_suspect_description">
        <input type="hidden" value="<?php echo $req['content'] ?>" name="delete_content">
        <div style="text-align: center" class="mb-3 col-xl-10 col-md-10 mb-10">
          <?php
          if ($_SESSION['grade'] == "Administrateur" || $_SESSION['grade'] == 'Commandant' || $_SESSION['grade'] == 'Capitaine' || $_SESSION['grade'] == 'Shériff' || $_SESSION['grade'] == 'Shériff-Adjoint' || $_SESSION['grade'] == 'Gouverneur') {
            echo '<button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" name="delete-closed-complaint-button">
            Supprimer
          </button>';
          }
          ?>
        </div>
      </form>
    </div>
  </div>
</div>
<script>

    $('#showUpdateCriminalRecord').on('click', function () {
        var show = document.getElementById('updateCriminalRecord');
        show.style.display = "block";
        var hide = document.getElementById('showUpdateCriminalRecord');
        hide.className = "d-none-btn";
    })
</script>
