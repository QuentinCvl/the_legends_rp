<div class="container">
  <div class="row header">
    <nav class="navbar navbar-expand-lg navbar-light">

      <button class="navbar-toggler hamburger" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse hamburger-item" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Acceuil<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
              Postuler
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="index.php?page=lspd_recruitment">LSPD</a>
              <a class="dropdown-item" href="index.php?page=lssd_recruitment">LSSD</a>
              <a class="dropdown-item" href="index.php?page=gouv_recruitment">Gourvernement</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <div id="logo">
      <img src="includes/img/logo.png" alt="logo">
    </div>

  </div>

  <div class="row main">
    <div id="intro">
      <h2>Connexion au Panel</h2>
      <p>Cette plateforme est exclusivement réservée aux membres du LSPD, LSSD et du Gouvernement.
        Si vous êtes étranger à ces organisations, vous n'avez pas l'autorisation de consulter
        les données répertoriées sur ce site.</p>
      <p>Le cas échéant, si vous n'avez pas reçu votre identifiant, que ce dernier est non fonctionnel,
        ou bien que vous êtes une autorité compétente de Los Santos,
        veuillez contacter Shadowwera <strong>(daryl_trager@discord.gg)</strong>.</p>
    </div>
    <?php
    if (isset($_POST['username']) && isset($_POST['password'])) {
      $new_user = new Users($_POST['username'], $_POST['password']);
      $new_user->login();
    }
    ?>
    <?php
    if (!isset($_SESSION['username'])) {
      echo '<div class="alert alert-danger" role="alert">' .
          "Vous n'êtes pas connecté, connectez vous pour accéder au panel" .
          '</div>
                <form method="post" action="index.php">
            <div style="text-align: center">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">
                        <img src="https://img.icons8.com/metro/26/000000/name.png" alt="user_icon"/>
                    </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Username" aria-label="Username"
                           aria-describedby="basic-addon1" name="username">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">
                        <img src="https://img.icons8.com/metro/26/000000/password.png"  alt="password_icon"/>
                    </span>
                    </div>
                    <input type="password" class="form-control" placeholder="Password" aria-label="Username"
                           aria-describedby="basic-addon1" name="password">
                </div>
                <button type="submit" class="btn btn-secondary">Se Connecter</button>
            </div>
        </form>';
    } else {
      echo '<div class="alert alert-success" role="alert">' .
          "Vous êtes connecté, cliquer ci-dessous pour accéder au panel" .
          '</div>
          <a href="index.php" style="margin: 1% auto;"><button type="button" class="btn btn-success">Panel</button></a>';
    }
    ?>
  </div>

</div>
